//
//  ViewController.h
//  acelerometro
//
//  Created by Hibran on 25/02/13.
//  Copyright (c) 2013 Hibran Martinez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>

@interface ViewController : UIViewController <UITextFieldDelegate,  UIAccelerometerDelegate, CLLocationManagerDelegate> {
    
    float valorY;
    float valorX;
    int cadena2;
    int numero;
    int numeroImagen;
    CLLocationManager *locationManager;
    CLLocation *velocidadActual;

}

-(void)awakeAcelerometro;
-(void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration; 
- (IBAction)escribirMensaje:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *mensaje;
@property (weak, nonatomic) IBOutlet UILabel *respondedor;
@property (weak, nonatomic) IBOutlet UIImageView *imagen;
@property (weak, nonatomic) IBOutlet UILabel *labelY;
@property (weak, nonatomic) IBOutlet UILabel *labelX;
@property (strong, nonatomic) IBOutlet CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UILabel *velocidad;



@end
