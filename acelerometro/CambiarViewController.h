//
//  CambiarViewController.h
//  acelerometro
//
//  Created by Hibran on 07/03/13.
//  Copyright (c) 2013 Hibran Martinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CambiarViewController : UIViewController <UIAccelerometerDelegate>

@end
