//
//  ViewController.m
//  acelerometro
//
//  Created by Hibran on 25/02/13.
//  Copyright (c) 2013 Hibran Martinez. All rights reserved.
//

#import "ViewController.h"
#import "CambiarViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize locationManager;

double angleY;
double angleX;
int numero;
int cadena2;
int numeroImagen;

//Primero ver si a es igual que b.
//si no es igual, entonces igualar b = a.
//si es igual que mande un mensaje en la pantalla que tarde 15 segundos.
//TODO se ejecuta cada 0.5 segundos.

- (void)viewDidLoad
{
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
    [self awakeAcelerometro];
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)awakeAcelerometro{
    //iniciando el acelerometro
    [[UIAccelerometer sharedAccelerometer] setUpdateInterval:45.0/60.0];
    [[UIAccelerometer sharedAccelerometer] setDelegate:self];
    NSLog(@"Inicia el accelerometro ");
}

//Movimiento del acelerometro con intervalo de 0.5 segundos
-(void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration{
    //Aceleración
    valorY = acceleration.y * 4;
    valorX = acceleration.x * 4;
    
    double angleY = atan(acceleration.y) * 180.0f / 3.14159f;
    double angleX = atan(acceleration.x) * 180.0f / 3.14159f;
    _labelY.text = [NSString stringWithFormat:@"%.2g", angleY];
    _labelX.text = [NSString stringWithFormat:@"%.2g", angleX];
    
    numero= [_mensaje.text length];
    NSLog(@"El valor de la cadena es: %i", numero);
    
    
    if(velocidadActual.speed <= 5)
    {
        _velocidad.text = @"Es cero";
    }
    else{
        _velocidad.text = [[NSString alloc] initWithFormat:@"%f",velocidadActual.speed];
        if(angleY >= -35 && angleY <=35)
        {
            //No se hace nada mientras es mayor que -30
            
        }
        else{
            if(numero > 0)
            {
                if(numero == cadena2)
                {
                    
                    //Si se llega a una posición menor a -30, que se muestre un mensaje
                    CambiarViewController * segundaVista = [self.storyboard instantiateViewControllerWithIdentifier:@"mensaje1"];
                    [self.navigationController pushViewController:segundaVista animated:YES];
                    
                }
                else
                {
                    cadena2 = numero;
                }
            }
            
        }
        if(angleX >= -35 && angleX <=35)
        {
            //No se hace nada mientras es mayor que -30
            
        }
        else{
            if(numero > 0)
            {
                if(numero == cadena2)
                {
                    
                    //Si se llega a una posición menor a -30, que se muestre un mensaje
                    CambiarViewController * segundaVista = [self.storyboard instantiateViewControllerWithIdentifier:@"mensaje1"];
                    [self.navigationController pushViewController:segundaVista animated:YES];
                    
                }
                else
                {
                    cadena2 = numero;
                }
            }
        }
    }
    
}

- (IBAction)escribirMensaje:(id)sender {
    MFMessageComposeViewController *mensajero= [MFMessageComposeViewController alloc];
    mensajero = [[MFMessageComposeViewController alloc] init] ;
    [mensajero presentViewController:self animated:YES completion:nil];
    
    
    if (![MFMessageComposeViewController canSendText]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not Available" message:@"SMS/iMessage sharing is not available on your device." delegate:mensajero cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [mensajero setBody:@"Texto inicial en la pantalla de envío de mensajes automáticos"];
    
        numero = [mensajero.body length];
        NSLog(@"Caracteres: %i", numero);
    
    
    //Iniciamos el acelerometro cuando llamamos la vista del mensaje
    //[self awakeAcelerometro];

}

-(void)viewDidAppear:(BOOL)animated{
    [self awakeAcelerometro];
    [self.navigationController setNavigationBarHidden:NO];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [_mensaje resignFirstResponder];
    return YES;
    
}

-(BOOL)shouldAutorotate{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{

    /*
    if(newLocation.speed <= 0)
    {
        _velocidad.text = @"Es cero";
    }
    else{
        _velocidad.text = [[NSString alloc] initWithFormat:@"%f",newLocation.speed];
    }
     */
    velocidadActual = newLocation;
}


@end







