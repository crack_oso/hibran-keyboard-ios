//
//  CambiarViewController.m
//  acelerometro
//
//  Created by Hibran on 07/03/13.
//  Copyright (c) 2013 Hibran Martinez. All rights reserved.
//

#import "CambiarViewController.h"
#import "ViewController.h"

@interface CambiarViewController ()

@end

@implementation CambiarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)awakeAcelerometro{
    //iniciando el acelerometro
    [[UIAccelerometer sharedAccelerometer] setUpdateInterval:45.0/60.0];
    [[UIAccelerometer sharedAccelerometer] setDelegate:self];
}

//Movimiento del acelerometro
-(void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration{

    //Aceleración
    float valorY;
    valorY = acceleration.y * 4;
    
    double angleY = atan(acceleration.y) * 180.0f / 3.14159f;
    double angleX = atan(acceleration.x) * 180.0f / 3.14159f;

    UIApplication* application = [UIApplication sharedApplication];
    if(application.statusBarOrientation == UIInterfaceOrientationPortrait)
    {
        if(angleY <= -34 || angleY >=34)
        {
        }
        else{
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    else{
        if(angleX <= -34 || angleX >=34)
        {
        }
        else{
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }

//dissmiss
    
}

-(void)viewDidAppear:(BOOL)animated{
    [self awakeAcelerometro];
    
}

@end
