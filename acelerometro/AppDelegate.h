//
//  AppDelegate.h
//  acelerometro
//
//  Created by Hibran on 25/02/13.
//  Copyright (c) 2013 Hibran Martinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
