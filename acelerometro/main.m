//
//  main.m
//  acelerometro
//
//  Created by Hibran on 25/02/13.
//  Copyright (c) 2013 Hibran Martinez. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
